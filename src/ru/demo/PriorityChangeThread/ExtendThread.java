package ru.demo.PriorityChangeThread;

public class ExtendThread extends Thread {

    private static final int REPEAT_COUNTER = 100000;

    @Override
    public void run() {
        try {
            for (int counter = 0; counter < REPEAT_COUNTER; counter++) {
                if (counter == REPEAT_COUNTER / 2) {
                    int currentPriority = currentThread().getPriority();
                    if (currentThread().getPriority() == 10) {
                        currentThread().setPriority(1);
                    } else {
                        currentThread().setPriority(10);
                    }
                    System.out.println("Приориет потока " + currentThread() + " изменен! " + currentPriority + " >> " + currentThread().getPriority());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Поток - " + currentThread().toString() + " выполнен");
    }
}
