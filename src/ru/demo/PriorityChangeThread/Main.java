package ru.demo.PriorityChangeThread;

public class Main {

    public static void main(String[] args) {
        Thread firstThread = new ExtendThread();
        Thread secondThread = new ExtendThread();
        firstThread.setPriority(1);
        secondThread.setPriority(10);
        firstThread.start();
        secondThread.start();
        try {
            firstThread.join();
            secondThread.join();
        } catch (InterruptedException e) {}
        System.out.println("Выполнение потоков окончено!");
    }
}
