package ru.demo.PicturesThread;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GeneralSiteLoader extends SiteObject {

    ArrayList<URL> intermediateSitesURL;


    public GeneralSiteLoader(URL url) {
        super(url, SiteType.GeneralSiteLoader);
        start();
    }

    @Override
    public void siteFunction() {
        contentSearcher(SiteObject.INTRMDT_LINK_PATTERN);
        SitesOperations.siteLoader(intermediateSitesURL, SiteType.IntermediateSiteLoader);
    }

    public void contentSearcher(Pattern intermediatePattern) {
        ArrayList<URL> sitesURL = new ArrayList<>();
        Matcher matcher = intermediatePattern.matcher(pageHyperText);
        while (matcher.find()) {
            try {
                sitesURL.add(new URL(matcher.group()));
            } catch (MalformedURLException e) {
                System.out.println("Ошибка \"URL_CREATE\" из \"SiteObject.sitesUrlSearcher\"");
            }
        }
        this.intermediateSitesURL = sitesURL;
    }

}
