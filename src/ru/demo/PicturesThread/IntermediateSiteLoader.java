package ru.demo.PicturesThread;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IntermediateSiteLoader extends SiteObject {

    private URL searchablePictureURL;

    public IntermediateSiteLoader(URL url) {
        super(url, SiteType.IntermediateSiteLoader);
        start();
    }

    @Override
    public void siteFunction() {
        contentSearcher(SiteObject.PCTR_PATTERN);
        new PictureLoader(searchablePictureURL, Main.PIC_SAVE_PATH);
    }


    public void contentSearcher(Pattern picturePattern) {
        Matcher pictureMatcher = picturePattern.matcher(pageHyperText);
        if (pictureMatcher.find()) {
            try {
                searchablePictureURL = new URL(pictureMatcher.group());
            } catch (MalformedURLException e) {
                System.out.println("\"URL_CREATE_ERROR\" from SiteObject.pictureSearcher");
            }
        } else {
            System.out.println("Картинка не найдена!");
        }
    }
}
