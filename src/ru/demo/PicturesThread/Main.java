package ru.demo.PicturesThread;

import java.net.URL;
import java.util.ArrayList;

public class Main {

    private static final String LINKS_TXT = "src\\ru\\demo\\PicturesThread\\files\\links.txt";
    public static final String PIC_SAVE_PATH = "src\\ru\\demo\\PicturesThread\\files\\pictures\\";

    public static void main(String[] args) {
        ArrayList<URL> links;
        links = SitesOperations.scanURLS(LINKS_TXT);
        SitesOperations.siteLoader(links, SiteType.GeneralSiteLoader);
    }
}
