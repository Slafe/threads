package ru.demo.PicturesThread;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class SitesOperations {

    public static ArrayList<URL> scanURLS(String filePath) {
        File file = new File(filePath);
        ArrayList<URL> list = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String currentLine = "";
            try {
                while ((currentLine = reader.readLine()) != null) {
                    list.add(new URL(currentLine));
                }
            } catch (MalformedURLException e) {
                System.out.println("Ошибка \"URL_CREATE\"; Строка - " + currentLine);
            }
        } catch (IOException e) {
             System.out.println("Ошибка \"READER_CREATE\"; Невозможно создать объект BufferedReader. Путь - " + file);
        }
        return list;
    }

    public static void siteLoader(ArrayList<URL> sites, SiteType type) {
        switch (type) {
            case GeneralSiteLoader:
                for (URL siteURL : sites) {
                    new GeneralSiteLoader(siteURL);
                }
                break;
            case IntermediateSiteLoader:
                for (URL siteURL : sites) {
                    new IntermediateSiteLoader(siteURL);
                }
        }
    }
}
