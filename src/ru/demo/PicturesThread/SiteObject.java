package ru.demo.PicturesThread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class SiteObject extends Thread {

    public static final Pattern INTRMDT_LINK_PATTERN = Pattern.compile("(?<=href=\")[^\"]*(\\.(jpg|png))(?=\")");
    public static final Pattern PCTR_PATTERN = Pattern.compile("(?<=src=\")[^\"]*(\\.(jpg|png))(?=\")");

    String pageHyperText;
    private URL siteURL;
    SiteType siteType;

    public SiteObject(URL url, SiteType loaderType) {
        siteURL = url;
        siteType = loaderType;
    }

    public abstract void contentSearcher(Pattern searchableContentPattern);

    public abstract void siteFunction();

    @Override
    public void run() {
        pageHyperText = htmlCodeReader(siteURL);
        siteFunction();
    }

    public String htmlCodeReader(URL siteURL) {
        String pageHyperText = "";
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(siteURL.openStream()))) {
            pageHyperText = bufferedReader.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            System.out.println("\"IOException\" from \"htmlCodeReader\"");
        }
        return pageHyperText;
    }
}