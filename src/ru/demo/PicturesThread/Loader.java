package ru.demo.PicturesThread;

import java.net.URL;
import java.lang.String;


public abstract class Loader extends Thread {

    URL url;
    String savePath;
//    String name;// rename
//    LoadableContentType type;
//    String format;

    public Loader(URL url, String savePath) {
        this.url = url;
        this.savePath = savePath;
        start();
    }

    @Override
    public void run() {
        loadContent();
    }

    public String formatIdentifier() {
        String urlString = url.toString();
        return urlString.substring(urlString.length() - 4, urlString.length());
    }

    public abstract void loadContent();
}
