package ru.demo.PicturesThread;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.atomic.AtomicInteger;

public class PictureLoader extends Loader {

    private static AtomicInteger counter = new AtomicInteger(1);

    private final String contentName = "picture";
    // private final LoadableContentType type = LoadableContentType.PictureLoader;
    String format;

    public PictureLoader(URL url, String savePath) {
        super(url, savePath);
        format = formatIdentifier();
    }

    public void loadContent() {
        try {
            ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
            FileOutputStream stream = new FileOutputStream(savePath + contentName + counter + format);
            System.out.println("Loading " + contentName + counter + " - " + url);
            synchronized (this) {
                counter.incrementAndGet();
            }
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            stream.close();
            byteChannel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

