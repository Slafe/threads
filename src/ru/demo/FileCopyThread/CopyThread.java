package ru.demo.FileCopyThread;

import java.io.*;

/**
 * Класс CopyThread
 * Реализует копирование данных из одного файла в другой
 * Поля класса:
 *  - reader (объект BufferedReader, позволяющий считывать данные из файла)
 *  - writer (объект BufferedWriter, позволяющий записывать данные в файл)
 *  - duration (переменная long, означающая время, за которое поток завершил свою работу)
 */

public class CopyThread extends Thread {
    private String readerPath;
    private String writerPath;
    private long duration;

    public CopyThread(String OUT_COPY_FILE, String IN_COPY_FILE, String name) throws IOException {
        readerPath = OUT_COPY_FILE;
        writerPath = IN_COPY_FILE;
        this.setName(name);
    }

    @Override
    public void run() {

        System.out.println("Поток " + currentThread() + " начал работу");
        try {
        BufferedReader reader = new BufferedReader(new FileReader(readerPath));
        BufferedWriter writer = new BufferedWriter(new FileWriter(writerPath));
        long startTime = System.currentTimeMillis();
        String line;
            while ((line = reader.readLine()) != null) {
                writer.write(line);
            }
            reader.close();
            writer.close();
            duration = System.currentTimeMillis() - startTime;
            System.out.println("Поток " + currentThread() + " завершил работу за " + duration + " ms");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
