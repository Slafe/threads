package ru.demo.FileCopyThread;

import java.io.IOException;

public class Main {
    private static final String FIRST_FILE_PATH = "src\\ru\\demo\\FileCopyThread\\files\\firstFile.txt";
    // Первый файл - файл, из которого будут считываться данные
    private static final String SECOND_FILE_PATH = "src\\ru\\demo\\FileCopyThread\\files\\secondFile.txt";
    private static final String THIRD_FILE_PATH = "src\\ru\\demo\\FileCopyThread\\files\\thirdFile.txt";
    // Второй и третий файлы - файлы, в которые будут записываться данные из первого файла

    /**
     * Метод main()
     * Данный метод является началом программмы и запускает методы soloLaunch и multiLaunch
     * @param args
     */
    public static void main(String[] args) {
        try {
            CopyThread soloFirstThread = new CopyThread(FIRST_FILE_PATH, SECOND_FILE_PATH, "soloFirstThread");
            CopyThread soloSecondThread = new CopyThread(FIRST_FILE_PATH, THIRD_FILE_PATH, "soloSecondThread");
            soloLaunch(soloFirstThread, soloSecondThread);
            CopyThread multiFirstThread = new CopyThread(FIRST_FILE_PATH, SECOND_FILE_PATH, "multiFirstThread");
            CopyThread multiSecondThread = new CopyThread(FIRST_FILE_PATH, THIRD_FILE_PATH, "multiSecondThread");
            multiLaunch(multiFirstThread, multiSecondThread);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Метод soloLaunch
     * Данный метод реализует последовательное выполнение потоков, которые копируют данные из одного файла и записывают в другой
     * Примерное время выполнения данного потока с текущими данными 70 - 110 ms
     * @param firstThread
     * @param secondThread
     */
    private static void soloLaunch(CopyThread firstThread, CopyThread secondThread) {
        System.out.println("Запуск soloLaunch...");
        long startTime = System.currentTimeMillis();
        try {
            firstThread.start();
            firstThread.join();
            secondThread.start();
            secondThread.join();
            long generalTime = System.currentTimeMillis() - startTime;
            System.out.println("Общее время работы soloLaunch - " + generalTime + " ms");
        } catch (InterruptedException e) {}
    }


    /**
     * Метод multiLaunch
     * Данный метод реализует параллельное выполнение потоков, которые копируют данные из одного файла и записывают в другой
     * Примерное время выполнения данного потока с текущими данными 8 - 13 ms
     * @param firstThread
     * @param secondThread
     */
    private static void multiLaunch(CopyThread firstThread, CopyThread secondThread) {
        System.out.println("Запуск multiLaunch...");
        long startTime = System.currentTimeMillis();
        try {
            firstThread.start();
            secondThread.start();
            firstThread.join();
            secondThread.join();
            long generalTime = System.currentTimeMillis() - startTime;
            System.out.println("Общее время работы multiLaunch - " + generalTime + " ms");
        } catch (InterruptedException e) {}
    }
}
