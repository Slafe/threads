package ru.demo.MusicThread;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;

/**
 * Проект MusicThread
 * Данный проект разработан с целью скачивания музыки с определенным пользователем сайтом(zvonko.me)
 * Основными функциями программы являются:
 *  - Считывание ссылок сайтов, с которых необходимо скачать музыку
 *  - Загрузка html кода сайтов в отдельном потоке
 *  - Нахождение ссылок на скачивание музыки из html кода
 *  - Загрузка музыки в отдельном потоке
 *
 *  @author Anthony 1520
 *
 * 1_01
 */

/**
 * Класс Main(класс старта работы программы)
 *
 * Методы:
 *  - main(String[] args)
 *  - loadSites(ArrayList<URL> links) - осуществляет загрузку сайтов в отдельном потоке SiteObject
 *
 * Функции:
 *  - Считывание ссылок сайтов из отдельного файла
 *  - Запуск потоков SiteObject
 */
public class Main {

    private static final String LINKS_TXT = "src\\ru\\demo\\MusicThread\\files\\links.txt";
    public static final String MUSIC_SAVE_PATH = "src\\ru\\demo\\MusicThread\\files\\music\\";

    public static void main(String[] args) {
        ArrayList<URL> links;
        links = FileOperations.scanURLS(new File(LINKS_TXT));
        loadSites(links);
    }

    /**
     * Метод loadSites
     * Осуществляет загрузку сайтов в отдельном потоке SiteObject
     * @param links - список URL-ссылок сайтов
     */
    private static void loadSites(ArrayList<URL> links) {
        for (URL currentURL : links) {
            new SiteLoader(currentURL);
        }
    }
}

