package ru.demo.MusicThread;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Класс MusicLoader
 * Класс предназначен для скачивание файла типа "mp3" в отдельном потоке
 * Функции:
 * - Скачивание музыки по URL-ссылке
 * - Создание файлов типа ".mp3"
 *
 * 1_04
 */
public class MusicLoader extends Thread {

    private String pathToSave = "src\\ru\\demo\\MusicThread\\files\\music\\";
    private URL url;
    private static AtomicInteger counter = new AtomicInteger(1);

    /**
     * Конструктор класса MusicLoader
     * Функции:
     *  - Инициализирует поля "url" и "counter" текущего класса
     *  - Запускает поток для текущего объекта
     * Параметры:
     * @param url - ссылка на скачивание музыки
     */
    public MusicLoader(URL url, String filePath) {
        this.url = url;
        this.pathToSave = filePath;
        this.start();
    }


    /**
     * Тело потока класса MusicLoader
     */
    @Override
    public void run() {
        try {
            ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
            FileOutputStream stream = new FileOutputStream(pathToSave + "music" + counter + ".mp3");
            counter.incrementAndGet();
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            stream.close();
            byteChannel.close();
        } catch (IOException e) {
        }
    }
}
