package ru.demo.MusicThread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Класс SiteObject
 * Методы:
 * - loadMusic() - запуск потоков загрузки музыки (MusicLoader.start())
 * - searchMusicLinks(String pageHyperText) - осуществляет поиск ссылок на скачивание музыки среди предоставленного
 * html-кода
 * Функции:
 * - Загрузка html кода страницы сайта
 * - Поиск ссылок на скачивание музыка среди html-кода
 * - Запуск потоков загрузки музыки(MusicLoader.start())
 *
 * 1_03
 */
public class SiteLoader extends Thread {

    private URL url;
    private ArrayList<URL> musicArray = new ArrayList<>();

    /**
     * Конструктор класса SiteObject
     * Осуществляет иницализацию поля url и запуск потока собственного класса
     * @param url - ссылка на предполагаемый сайт
     */
    public SiteLoader(URL url) {
        this.url = url;
        this.start();
    }

    /**
     * Тело потока класса SiteObject
     * Осущесвляет запуск методов:
     * - searchMusicLinks
     * - loadMusic
     */
    @Override
    public void run() {
        String pageHyperText = htmlCodeReader(url);
        searchMusicLinks(pageHyperText);
        loadMusic();
    }

    private String htmlCodeReader(URL siteURL) {
        String pageHyperText = "";
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(siteURL.openStream()))) {
            pageHyperText = bufferedReader.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            System.out.println("\"IOException\"(1_03_1) from \"htmlCodeReader\"");
        }
        return pageHyperText;
    }

    /**
     * Метод loadMusic
     * Создает объекты MusicLoader, используя URL-ссылки коллекции musicArray
     */
    private void loadMusic() {
        for (URL musicLink : musicArray) {
            new MusicLoader(musicLink, Main.MUSIC_SAVE_PATH);
        }
    }

    /**
     * Метод searchMusicLinks(String pageHyperText)
     * Осуществляет поиск ссылок на скачивание музыки, основываясь на регулярном выражении
     * Параметры :
     * @param pageHyperText - html-код страницы, в котором будет осуществлятся поиск ссылок
     * на скачивание музыки
     */
    private void searchMusicLinks(String pageHyperText) {
        Pattern pattern = Pattern.compile("(?<=data-url=\")[^\"]*(?=\")");
        Matcher matcher = pattern.matcher(pageHyperText);
        while (matcher.find()) {
            try {
                musicArray.add(new URL(matcher.group()));
            } catch (MalformedURLException e) {
                System.out.println("Ошибка \"URL_CREATE\" из \"searchMusicLinks\"");
            }
        }
    }
}
