package ru.demo.MusicThread;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Класс FileOperations
 * Содержит методы для работы с файлами и их содержимым
 * Методы:
 * - scanURLS(File filePath) - метод, возвращающий ArrayList<URL>, наполненный данными типа URL, считанных из файла с путем FilePath
 *
 * 1_02
 */
public class FileOperations {

    /**
     * Метод scanURLS
     * Построчно считывает данные из файла и заносит их в коллекцию
     * @param filePath - путь
     * @return ArrayList<URL> - коллекция, содержащая ссылки на сайты
     */
    public static ArrayList<URL> scanURLS(File filePath) {
        ArrayList<URL> list = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String currentLine = "";
            try {
                while ((currentLine = reader.readLine()) != null) {
                    list.add(new URL(currentLine));
                }
            } catch (MalformedURLException e) {
                System.out.println("Ошибка \"URL_CREATE\"(1_02_1); Строка - " + currentLine);
            } catch (IOException e) {
                System.out.println("Ошибка \"INVALID_READER\"(1_02_2); Невозмонжо использовать BufferedReader " + reader.toString());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Ошибка \"READER_CREATE\"(1_02_3); Невозможно создать объект BufferedReader. Путь - " + filePath);
        } catch (IOException e) {
            System.out.println("\"IO_EXCEPTION\"(1_02_4)");
        }
        return list;
    }
}
