package ru.demo.IOFileDataThread;

import java.io.*;
import java.util.ArrayList;

public class FileInput extends Thread {
    private ArrayList<String> fileData;
    BufferedReader reader;

    public FileInput(String name) throws FileNotFoundException {
        super(name);
        fileData = new ArrayList<>();
        reader = new BufferedReader(new FileReader(name));
    }

    public ArrayList<String> getFileData() {
        return fileData;
    }

    @Override
    public void run() {
        String line;
        try {
            while((line = reader.readLine()) != null) {
                fileData.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
