package ru.demo.IOFileDataThread;

import java.io.*;
import java.util.ArrayList;

public class Main {

    private static final String FIRST_FILE_PATH = "src\\ru\\demo\\IOFileDataThread\\files\\firstFile.txt";
    private static final String SECOND_FILE_PATH = "src\\ru\\demo\\IOFileDataThread\\files\\secondFile.txt";
    // Первый и второй файл - файлы, из которых будут считываться данные
    private static final String THIRD_FILE_PATH = "src\\ru\\demo\\IOFileDataThread\\files\\thirdFile.txt";
    // Третий файл - файл, в который будут записываться поочередно строки из первого и второго файлов

    public static void main(String[] args) {
        try {
            FileInput firstFileInput = new FileInput(FIRST_FILE_PATH);
            FileInput secondFileInput = new FileInput(SECOND_FILE_PATH);
            BufferedWriter writer = new BufferedWriter(new FileWriter(THIRD_FILE_PATH));
            firstFileInput.start();
            secondFileInput.start();
            firstFileInput.join();
            secondFileInput.join();
            FileInput currentInput = firstFileInput;
            ArrayList<String> firstArray = firstFileInput.getFileData();
            ArrayList<String> secondArray = secondFileInput.getFileData();
            while ((firstArray.size() != 0) | (secondArray.size() != 0)) {
                    if (currentInput == firstFileInput) {
                        writer.write(firstArray.get(0) + "\n");
                        System.out.println(firstArray.get(0));
                        firstArray.remove(0);
                        currentInput = secondFileInput;
                    } else {
                        writer.write(secondArray.get(0) + "\n");
                        System.out.println(secondArray.get(0));
                        secondArray.remove(0);
                        currentInput = firstFileInput;
                    }
                }
            writer.flush();
            writer.close();
        } catch (java.io.FileNotFoundException FILE_NOT_FOUND) {
            FILE_NOT_FOUND.printStackTrace();
        } catch (InterruptedException e) {}
        catch (IOException e) {}
    }

}
