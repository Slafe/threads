package ru.demo.IncrementationThread;

public class Main {
    public static volatile int counter = 0;

    /**
     * Класс Main
     * Программа предназначена для одновременного икрементирования счетчика класса Main,
     * исключая потерю данных и конфликты потоков
     * @author Isheykin 1520
     */

    public static void main(String[] args) {
        IncrementationThread firstThread = new IncrementationThread();
        IncrementationThread secondThread = new IncrementationThread();
        firstThread.start();
        secondThread.start();
        while (firstThread.isAlive() | secondThread.isAlive()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {}
        }
        System.out.println("Выполнение потоков окончено! Общий счетчик = " + counter + " повторений инкрементирования");
    }
}
