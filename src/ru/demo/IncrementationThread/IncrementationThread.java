package ru.demo.IncrementationThread;

public class IncrementationThread extends Thread {
    /**
     * Класс IncrementationThread
     * Осуществляет инкремент поля класса "counter" класса Main
     */
    private int INCREMENT_TO = 1000;

    @Override
    public void run() {
        int localCounter;
        for (localCounter = 0; localCounter < INCREMENT_TO; localCounter++) {
                Main.counter++;
        }
        System.out.println(Thread.currentThread() + " окончил работу с " + localCounter);
    }
}
