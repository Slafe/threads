package ru.demo.DemonstrateProjects.HelloThread;

/**
 * Создание подкласса Thread
 *
 * Класс Thread сам реализует интерфейс Runnable,
 * хотя его метод run() ничего не делает.
 * Подкласс класса Thread может обеспечить собственную реализацию метода run()
 */

public class HelloThread extends Thread {
    public void run() {
        System.out.println("Hello from thread!");
    }

    public static void main(String[] args) {
        (new HelloThread()).start();
        System.out.println("Hello from main thread!");
    }
 }
