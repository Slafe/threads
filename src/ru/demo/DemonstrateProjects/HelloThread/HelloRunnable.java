package ru.demo.DemonstrateProjects.HelloThread;

/**
 * Реализация интерфейса Runnable
 *
 * Интерфейс Runnable отпределяет один метод run
 * предназначенный для размещения кода, исполняемого в потоке.
 * Runnable-объект пересылается в конструктор Thread
 * и с помощью метода start() поток запускается
 */

public class HelloRunnable implements Runnable {
    public void run() {
        (new Thread(new AnotherRunnble())).start();
        System.out.println("Hello from a thread!");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {

        }
        System.out.println("Поток run окончен!");
    }

    public static void main(String[] args) {
        (new Thread(new HelloRunnable())).start();
        System.out.println("Hello from main thread!");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            System.out.println("Главный поток");
        }
        System.out.println("Поток main окончен!");
    }
}
